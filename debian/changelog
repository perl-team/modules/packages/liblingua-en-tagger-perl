liblingua-en-tagger-perl (0.31-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 27 Nov 2022 16:51:13 +0000

liblingua-en-tagger-perl (0.31-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Update standards version to 4.4.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 30 Jun 2022 10:09:40 +0100

liblingua-en-tagger-perl (0.31-1) unstable; urgency=medium

  * Import upstream version 0.31.
  * Update years of packaging copyright.
  * Annotate test-only build dependencies with <!nocheck>.
  * Declare compliance with Debian Policy 4.4.0.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.

 -- gregor herrmann <gregoa@debian.org>  Sat, 20 Jul 2019 16:47:15 -0300

liblingua-en-tagger-perl (0.30-1) unstable; urgency=medium

  * New upstream release.
  * Declare compliance with Debian Policy 4.2.1.

 -- gregor herrmann <gregoa@debian.org>  Sun, 30 Sep 2018 18:44:19 +0200

liblingua-en-tagger-perl (0.29-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * New upstream release.
  * Update upstream email address.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.1.4.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Fri, 22 Jun 2018 23:24:59 +0200

liblingua-en-tagger-perl (0.28-1) unstable; urgency=medium

  * New upstream release.
  * Drop patch use-nstore-for-datafiles, applied upstream.

 -- gregor herrmann <gregoa@debian.org>  Tue, 27 Dec 2016 01:53:56 +0100

liblingua-en-tagger-perl (0.27-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Chris Butler from Uploaders. Thanks for your work!
  * New upstream release.
    Fixes "Unescaped left brace in regex is deprecated"
    (Closes: #826483)
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 3.9.8.
  * Bump debhelper compatibility level to 9.

 -- gregor herrmann <gregoa@debian.org>  Fri, 23 Dec 2016 00:12:17 +0100

liblingua-en-tagger-perl (0.25-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Axel Beckert ]
  * Apply patch to debian/patches/use-nstore-for-datafiles by Reiner
    Herrmann to make build reproducible. (Closes: #787109)
  * Add debian/upstream/metadata
  * Imported upstream version 0.25
  * Apply wrap-and-sort.
  * Declare compliance with Debian Policy 3.9.6 (no other changes needed).
  * Mark package as autopkgtestable.

 -- Axel Beckert <abe@debian.org>  Sat, 30 May 2015 12:51:00 +0200

liblingua-en-tagger-perl (0.24-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Tue, 22 Oct 2013 20:18:17 +0200

liblingua-en-tagger-perl (0.23-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Sun, 09 Dec 2012 17:12:16 +0100

liblingua-en-tagger-perl (0.20-1) unstable; urgency=low

  [ Harlan Lieberman-Berg ]
  * New upstream release.
  * Remove patch applied upstream; refresh patch

  [ gregor herrmann ]
  * Drop another patch.
  * Drop build dependency on libtest-pod-perl.
  * Bump Standards-Version to 3.9.4 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Wed, 19 Sep 2012 19:24:26 +0200

liblingua-en-tagger-perl (0.18-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * New upstream release.
  * Refresh patches.
  * debian/copyright: update to Copyright-Format 1.0.
  * Update years of packaging copyright.
  * Bump Standards-Version to 3.9.3 (no changes).
  * Set debhelper compatibility level to 8.
  * Remove versions from (build) dependencies, all satisfied in oldstable.

 -- gregor herrmann <gregoa@debian.org>  Sat, 26 May 2012 21:11:25 +0200

liblingua-en-tagger-perl (0.16-1) unstable; urgency=low

  [ gregor herrmann ]
  * New upstream release.
  * Refresh patch fix-pod-errors.
  * Add /me to Uploaders.
  * Update debian/copyright to the information now included in the upstream
    source.
  * Adjust short description.

  [ Chris Butler ]
  * Added patch use-nstore-for-datafiles which makes the installed lexicon
    files architecture-independent by using nstore.

 -- Chris Butler <chrisb@debian.org>  Sat, 05 Jun 2010 12:13:01 +0100

liblingua-en-tagger-perl (0.15-1) unstable; urgency=low

  * Initial Release. (closes: #580275)
  * Added clarification of upstream copyright to debian/copyright.

 -- Chris Butler <chrisb@debian.org>  Sun, 09 May 2010 16:43:49 +0100
